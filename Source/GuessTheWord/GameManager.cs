
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Preferences;
using Android.Content.Res;
using Android.Graphics;
using Android.Media;
using Android.Graphics.Drawables;


namespace GuessTheWord
{


	public enum LevelHelp{
		reveal_letter =0,
		remove_letter =1,
		skip_level=2
	}

	public enum LevelType{
		Orignal = 0,
		Single = 1,
		Double = 2,
		Logos = 3,
        Flags = 4,
        Cars = 5,
        Cartoons = 6
	}

	public enum GameState{
		Menu=0,
		Selection=1,
		Level=2
	}


	class GameManager
	{
		public static int levelCounter=0;
		public static AssetManager assets=LayoutManager.activityLayout.Assets;
		public static int revealLetterCount=0;
		public static int removeLetterCount=0;
		public static Levels currentLevel;
		public static ISharedPreferences prefs;
		public static LevelType currentLevelType;
		public static GameState currentGameState;
		public static List<string> levelsDone;
		public static long totalScore=0;

		//>>>>>>>>>>LEVELS<<<<<<<<<<<//
	//	public static List<Levels> singleLevelList;
	//	public static List<Levels> doubleLevelList;
		public static List<Levels> levelList;

		//>>>>>>>>>LEVEL PACKAGES<<<<<<<<<//
	//	public static List<Levels>[] singleLevelPackageList;
	//	public static List<Levels>[] doubleLevelPackageList;
	//	public static List<Levels>[] orignalLevelPackageList;

		//public static int[] randomList;
		public static long points=0;
		public static InAppPurchase purchaser;

		public static int productID=0;

		public GameManager()
		{

			InitiateGame();
		}

		public static void InitiateGame ()
		{
			GetPrefrences ();
			LoadFonts();



			//GetRandomIntegerList(levelList.Count);

			LayoutManager.InitiateHeader();
			LayoutManager.InitiateMenuLayout();
		
		//	LayoutManager.InitiateLevelSelectionLayout(levelList,currentLevelType);
		
			SavePreferences();
			purchaser = new InAppPurchase();


		}

		private static void UpdateHeader (int id, long points)
		{

			TextView txt = LayoutManager.activityLayout.FindViewById<TextView> (754);
			txt.SetText ("LEVEL : " + id, TextView.BufferType.Spannable);
			
			TextView txt2 = LayoutManager.activityLayout.FindViewById<TextView> (757);
			txt2.SetText ("POINTS : " + points, TextView.BufferType.Spannable);
		}

	/*	public static void GetRandomIntegerList (int count)
		{
			if (isRandomBefore ()|| !(isOld(count))) {
				randomList = GetAllIntegersFromString(prefs.GetString("Random_Levels","none"));

			} else {
			
				ISharedPreferencesEditor edit = prefs.Edit();
				edit.PutString("Random_Levels",GenerateRandomNumberString(count));
				edit.Commit();
			}
		}

		public static string GenerateRandomNumberString (int count)
		{
			Random rnd = new Random ();
			var randomNumbers = Enumerable.Range (1 - 1, count - 1).OrderBy (i => rnd.Next ()).ToArray ();
			randomList = randomNumbers;
			string val ="";
			for(int p = 0;p<randomNumbers.Length;p++)
			{
				if(p==randomNumbers.Length-1)
					val+=randomList[p].ToString();
				else
					val+=randomList[p].ToString()+",";
			}
			return val;
		}

		public static bool isOld (int c)
		{
			int count = prefs.GetString ("Random_Levels", "none").Split (',').Length - 1;
			if (count + 1 < c) {
				return true;
			} else {
				return false;
			}
		}

		public static bool isRandomBefore ()
		{
			if (prefs.GetString ("Random_Levels", "none").Equals ("none")) {
				return false;
			} else {
				return true;
			}
		}*/

		public static bool isGotSharedPoints ()
		{
			if (prefs.GetString ("share_again", "false").Equals ("true")) {
				return true;
			} else {
				return false;
			}
		}

		private static List<string> GetValuesFromString (string val)
		{
			//Console.WriteLine(val);
				if(val.Equals("none"))
			   {
				List<string> val2  = new List<string>();
				val2.Add("none");
				return val2;//new List<string>().Add("none");
				}
				else
				{
					string[] str = val.Split ('.');
				totalScore=(long)(str.LongLength*100);
					return str.ToList();//nValues;
				}
		}

		public static bool IsCompletedBefore (string val)
		{
			foreach (string str in levelsDone) {
				if(str.Equals(val))
					return true;
			}
			return false;
		}

		private static string PutValuesFromString (List<string> listStr)
		{
			string val = string.Empty;
			if(listStr.Count>0)
			{
				for(int x=0;x<listStr.Count;x++)
				{
					if(x==(listStr.Count-1))
					{
						val+= listStr[x];
					}
					else
					{
					val+= listStr[x]+ ".";
					}
				}
			}
			return val;
		}

		public static void NextLevel ()
		{


			LayoutManager.ClearLayout ();
	//		if (levelCounter < levelList.Count)
	//			LayoutManager.DeployLayout (levelList [randomList[levelCounter++]]);


			SavePreferences();

		}

		public static void UsePoints (LevelHelp help)
		{
			switch (help) {
			case LevelHelp.reveal_letter:
				if(points>=100)
				{
				RevealLetter();
				points-=100;
				}
				else
					LayoutManager.BuyPointBox("Insufficiant Points...click buy button to buy points or wait evey hour to get 10 points");
				break;
			case LevelHelp.remove_letter:
				if(points>=100)
				{
				RemoveLetter();
				points-=100;
				}
				else
					LayoutManager.BuyPointBox("Insufficiant Points...click buy button to buy points or wait evey hour to get 10 points");
				break;
			case LevelHelp.skip_level:
				if(points>=350)
				{
				//	levelCounter++;
				NextLevel();
				points-=350;
				}
				else
					LayoutManager.BuyPointBox("Insufficiant Points...click buy button to buy points or wait evey hour to get 10 points");
				break;
			}
		
			UpdateStatus();
		}

		public static void UpdateStatus ()
		{
			if (currentLevel != null) {
				UpdateHeader (currentLevel.levelID, points);
			
				SavePreferences ();
			}
		}

		public static void SavePreferences ()
		{
			ISharedPreferencesEditor edit = prefs.Edit();
			edit.PutString("point",points.ToString());
		
			edit.PutString("levels_done",PutValuesFromString(levelsDone));
			edit.Commit();
		}

		public static void RevealLetter ()
		{
			Button btn = LayoutManager.activityLayout.FindViewById<Button> (200 + revealLetterCount);

			if (LayoutManager.Alphabets [revealLetterCount].alphabetBtnId !=0) {
				Button temp = LayoutManager.activityLayout.FindViewById<Button>(
					LayoutManager.Alphabets[revealLetterCount].alphabetBtnId);

				temp.Enabled=true;
				temp.Visibility=ViewStates.Visible;
			}

			if(string.IsNullOrEmpty(btn.Text))
				LayoutManager.spellCount++;

			LayoutManager.Alphabets[revealLetterCount].val=LayoutManager.currentLevelAnswer.ToUpper()[revealLetterCount];
			btn.SetText(LayoutManager.Alphabets[revealLetterCount].val.ToString(),TextView.BufferType.Normal);
			btn.Enabled=false;
			revealLetterCount++;

			if(revealLetterCount==LayoutManager.currentLevelAnswer.Length)
				LayoutManager.ShowAlert(LayoutManager.IsCorrect(LayoutManager.currentLevelAnswer));
		}

		public static void RemoveLetter ()
		{
			Button btn = LayoutManager.activityLayout.FindViewById<Button>(LayoutManager.ExtraButtons[removeLetterCount++]);
			btn.Enabled=false;
			btn.Visibility=ViewStates.Invisible;
		}



		public static void GetPrefrences()
		{
			levelsDone = new List<string>();
			PreferenceManager.SetDefaultValues(LayoutManager.activityLayout,
			                                                Resource.Xml.Preference,false);
			prefs = PreferenceManager.GetDefaultSharedPreferences(LayoutManager.activityLayout);
			points = Convert.ToInt64(prefs.GetString("point","0"));
			levelsDone = GetValuesFromString(prefs.GetString("levels_done","none"));
			foreach(string str in levelsDone)
				Console.WriteLine(str);
		}

		public static void GetAllLevel (LevelType lType,int index)
		{
			levelList = new List<Levels> ();
			int cnt=0;

			//AssetManager assets = LayoutManager.activityLayout.Assets;
			string[] values= assets.List(@"Levels/"+lType.ToString()+"/package"+index.ToString());


			//string[] values = assets.List(@"Levels");
			/*for(int o=0;o<(int)(values.Length/12)+1;o++)
			{

			for(int k=0;k<12;k++) {
			
					if(o==index)
					{
						if(cnt<values.Length)
						{
							bool comp = false;
							if(IsCompletedBefore(values[cnt]))
								comp=true;

						Levels lvl = new Levels(cnt+1,values[cnt],comp);
						levelList.Add(lvl);
						}
					}
					cnt++;
				}
			}*/
			for(int o=0;o<values.Length;o++)
			{
				bool comp = false;
				if(IsCompletedBefore(values[cnt]))
					comp=true;

				Levels lvl = new Levels(cnt+1,values[cnt],comp);
				levelList.Add(lvl);
				cnt++;

			}
		}

	

		public static void LoadFonts ()
		{
			Typeface ttf = Typeface.CreateFromAsset (assets, "Fonts/Chunk.otf");
			LayoutManager.font=ttf;
		}

		public static Bitmap[] GetImagesNameFromFolder (string word,LevelType lType)
		{
			Bitmap[] values = new Bitmap[4];
			string[] imgs = assets.List (@"Levels/"+lType.ToString()+"/package"+(LayoutManager.packageIndex).ToString()+"/"+ word );
			Console.WriteLine(imgs.Length);
			foreach(string str in imgs)
				Console.WriteLine(str);
            int k = 0;
			for (int i=0; i<imgs.Length; i++) {
			//	Console.WriteLine(imgs);
                if (!imgs[i].Equals("thumb"))
                {
                    values[k] = BitmapFactory.DecodeStream(assets.Open(@"Levels/" + lType.ToString() + "/package" + (LayoutManager.packageIndex).ToString() + "/" + word + "/" + imgs[i]));
                    k++;
                }                
			}
			return values;
		}

		public static Bitmap[] GetThumbnailFromAssets (string word, LevelType lType)
		{
			Bitmap[] values = new Bitmap[4];
			string[] imgs = assets.List (@"Levels/"+lType+"/package"+(LayoutManager.packageIndex).ToString()+"/"+ word +"/thumb");
			//	Console.WriteLine(imgs.Length);
			for (int i=0; i<imgs.Length; i++) {
				//	Console.WriteLine(imgs);

				values [i] = BitmapFactory.DecodeStream (assets.Open (@"Levels/"+lType+"/package"+(LayoutManager.packageIndex).ToString()+"/"+ word + "/thumb/" + imgs [i]));
			}
			return values;
		}



	}
}

