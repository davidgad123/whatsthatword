
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GuessTheWord
{
	class Levels
	{

		public int levelID{
			get;
			set;
		}
	
		public string word
		{
			get;set;
		}

		public bool isCompleted{
			get;set;
		}


		public Levels (int id,string word,bool isComp)
		{
			this.levelID = id;
			this.word = word;
			this.isCompleted = isComp;

		}

	}

}

