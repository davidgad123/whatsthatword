
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Preferences;

namespace GuessTheWord
{
	public class AppRater {
		private static string APP_TITLE = "Whats That Word";
		private static string APP_PNAME = "com.whatsthatword.game";
		
		//private static int DAYS_UNTIL_PROMPT = 3;
		private static int LAUNCHES_UNTIL_PROMPT = 3;
		
		public static void app_launched (Context mContext)
		{
			ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences (mContext);
			DateTime lastmillis = System.DateTime.Now;
			string lastLaunched = prefs.GetString ("last_launch", "0");
			try {
				lastmillis = Convert.ToDateTime (lastLaunched);
			} catch (System.Exception e) {
				Console.WriteLine(e.ToString());
			}
			//Console.WriteLine(lastmillis);
			ISharedPreferencesEditor editor1 = prefs.Edit ();
			if(!(prefs.GetString("Random_Levels","none").Equals("none")))
			{
				LayoutManager.MessageBox("Due to this major update, we are including a free 1000 points for users having to repeat levels. Hope you like the new changes. Thank you. Enjoy");
				GameManager.points+=1000;
				editor1.PutString("Random_Levels","none");

			}
			editor1.PutString ("last_launch", System.DateTime.Now.ToString ());
			editor1.Commit ();

			int hours = (int)((System.DateTime.Now.Subtract (lastmillis)).TotalHours);
			if (hours > 0) {
				//GameManager.points += 10 * hours;
				GameManager.UpdateStatus();
			}




			string val = prefs.GetString("dontshowagain","false") ;
			bool valu = false;
			if(val.Equals("false"))
				valu = false;
			else
				valu=true;


			Console.WriteLine(valu);
			if (valu) { return ; }
			
			ISharedPreferencesEditor editor = prefs.Edit();
			//editor.
			// Increment launch counter
			long launch_count = Convert.ToInt64(prefs.GetString("launch_count","0"))+1;
		//	Console.WriteLine(launch_count);//prefs.getLong("launch_count", 0) + 1;
			//editor.PutLong("launch_count",launch_count);//editor.putLong("launch_count", launch_count);
			editor.PutString("launch_count",launch_count.ToString());
			// Get date of first launch
		//	long date_firstLaunch = Convert.ToInt64( prefs.GetString("date_firstlaunch","0"));//prefs.GetLong("date_firstLaunch",0);
			//Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
		//	Console.WriteLine(date_firstLaunch);
		//	if (date_firstLaunch == 0) {
		//		date_firstLaunch = System.DateTime.Now.Millisecond;// .currentTimeMillis();
				//editor.PutLong("date_firstlaunch",date_firstLaunch);//editor.putLong("date_firstlaunch", date_firstLaunch);
			//	editor.PutString("date_firstlaunch",date_firstLaunch.ToString());
		//	}
			
			// Wait at least n days before opening
			if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
			//	if (System.DateTime.Now.Millisecond >= date_firstLaunch + 
			//	    (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
					showRateDialog(mContext,editor);
			//	}
			}
			//showRateDialog(mContext,editor);
			editor.Commit();//editor.commit();
		}   
		
		public static void showRateDialog(Context mContext,ISharedPreferencesEditor editor) {
			Dialog dialog = new Dialog(mContext);
			dialog.SetTitle("Rate " + APP_TITLE);
			
			LinearLayout ll = new LinearLayout(mContext);
			ll.Orientation = Orientation.Vertical;// .setOrientation(LinearLayout.VERTICAL);
			LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FillParent,
			                                                                LinearLayout.LayoutParams.WrapContent);
			TextView tv = new TextView(mContext);
			tv.SetText("If you enjoy using " + APP_TITLE + ", please take a moment to rate it. Thanks for your support!",TextView.BufferType.Normal);
			tv.SetWidth(240);
			tv.SetPadding(4, 0, 4, 10);
			ll.AddView(tv,param);
			
			Button b1 = new Button(mContext);

			b1.SetText("Rate us 5 to get 300 Points Free",TextView.BufferType.Normal);
			b1.Click+= delegate {
				if (editor != null) {
				GameManager.points+=300;
					GameManager.UpdateStatus();
					//editor.PutString("point",GameManager.points.ToString());
					//LayoutManager.UpdateHeader(GameManager.levelCounter,GameManager.points,false);
				mContext.StartActivity(new Intent(Intent.ActionView,Android.Net.Uri.Parse("market://details?id=" + APP_PNAME)));
				editor.PutString("dontshowagain","true");
				editor.Commit();
				}
				dialog.Dismiss();
			};       
			ll.AddView(b1,param);
			
			Button b2 = new Button(mContext);
			b2.SetText("Remind me later",TextView.BufferType.Normal);
			b2.Click+= delegate {
				if (editor != null) {
				editor.PutString("launch_count","0");
				editor.Commit();
				}
				dialog.Dismiss();
			}; 
			ll.AddView(b2,param);
			
			Button b3 = new Button(mContext);
			b3.SetText("No, thanks",TextView.BufferType.Normal);
			b3.Click+= delegate {
				if (editor != null) {
					editor.PutString("dontshowagain","true");
					//editor.PutBoolean("dontshowagain", true);
					editor.Commit();
				}
				dialog.Dismiss();
			}; 
			ll.AddView(b3,param);
			
			dialog.SetContentView(ll);        
			dialog.Show();        
		}
	}
	// see http://androidsnippets.com/prompt-engaged-users-to-rate-your-app-in-the-android-market-appirater
}

