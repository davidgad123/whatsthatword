
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GuessTheWord
{
	class HeyZap
	{
		public static string HeyZapName="com.heyzap.android";

		public static void LoadLeaderBoard (Context con,string val)
		{

			try
			{
				IntPtr TapForTap = JNIEnv.FindClass("com/heyzap/sdk/HeyzapLib");
		//		Console.WriteLine(TapForTap);
				IntPtr initialize  = JNIEnv.GetStaticMethodID(TapForTap,"showLeaderboards","(Landroid/content/Context;Ljava/lang/String;)V");
		//		Console.WriteLine(initialize);
				Java.Lang.String str = new Java.Lang.String(val);
				JNIEnv.CallStaticVoidMethod(TapForTap,initialize,new JValue(con),new JValue(str));

			}catch(System.Exception e){
				Console.WriteLine(e.ToString());
				DownloadHeyZap();
			}
		}

		public static void CheckIn(Context con)
		{
			try
			{

				IntPtr TapForTap = JNIEnv.FindClass("com/heyzap/sdk/HeyzapLib");
				//		Console.WriteLine(TapForTap);
				IntPtr initialize  = JNIEnv.GetStaticMethodID(TapForTap,"checkin","(Landroid/content/Context;)V");
			//			Console.WriteLine(initialize);
		//		Java.Lang.String str = new Java.Lang.String(val);
				JNIEnv.CallStaticVoidMethod(TapForTap,initialize,new JValue(con));
				
				
			}catch(System.Exception e){
				Console.WriteLine(e.ToString());
		//		LoadHeyZap(con,false);
			}
		}
		public static void DownloadHeyZap()
		{
			
			AlertDialog.Builder builder1 = new AlertDialog.Builder (LayoutManager.activityLayout);
			builder1.SetMessage ("Download HeyZap to use Leaderboard");
			builder1.SetPositiveButton ("Download", (sender,e) => { 
				LayoutManager.activityLayout.StartActivity(new Intent(Intent.ActionView,Android.Net.Uri.Parse("market://details?id=" + HeyZapName)));
				builder1.Dispose (); 
				
			}); 
			builder1.SetNegativeButton ("Cancel", (sender,e) => { 
				
				builder1.Dispose (); 
				
			}); 
			builder1.Show(); 
		}


		public static void LoadHeyZap(Context con,bool isShow)
		{

			try
			{
				IntPtr TapForTap = JNIEnv.FindClass("com/heyzap/sdk/HeyzapLib");

				IntPtr initialize  = JNIEnv.GetStaticMethodID(TapForTap,"load","(Landroid/content/Context;Z)V");
				Console.WriteLine(initialize);

				Java.Lang.Boolean b =new Java.Lang.Boolean(isShow);

				JNIEnv.CallStaticVoidMethod(TapForTap,initialize,new JValue(con),new JValue(b));
				Console.WriteLine("HEYZAP LOADED SUCCESSFULLY");
				
			}catch(System.Exception e){
				Console.WriteLine(e);
				
			}
		}

		public static void PutScore(Context con,string score,string displayScore,string levelId)
		{
			try
			{
				IntPtr TapForTap = JNIEnv.FindClass("com/heyzap/sdk/HeyzapLib");
				Console.WriteLine("Adress of the CLASS :"+TapForTap);
				IntPtr initialize  = JNIEnv.GetStaticMethodID(TapForTap,"submitScore","(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
				Console.WriteLine("Adress of the METHOD :"+initialize);
				Java.Lang.String str = new Java.Lang.String(levelId);
				Java.Lang.String str1 = new Java.Lang.String(score);
				Java.Lang.String str2 = new Java.Lang.String(displayScore);
				JNIEnv.CallStaticVoidMethod(TapForTap,initialize,new JValue(con),new JValue(str1),new JValue(str2),new JValue(str));
				
				
			}catch(System.Exception e){
				Console.WriteLine(e.ToString());
				Console.WriteLine("ERROR");
				
			}
		}
	}
}

