
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Views.Animations;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.Lang;
using Android.Content.Res;




namespace GuessTheWord
{
	static class LayoutManager
	{
		//>>>>>>> Initialize variables <<<<<<<<<//
		public static int spellCount=0;
		public static Button[] wordBtns = new Button[12];
		public static string currentLevelAnswer;
		public static Activity1 activityLayout = Activity1.MainlayoutActivity;
		public static LinearLayout layout =  activityLayout.FindViewById<LinearLayout>(Resource.Id.layout) ;
		public static TableLayout wordButtonLayout ;
		public static TableLayout wordlayout ;
		public static TableLayout imgTable ;
		public static int[] ExtraButtons;
		public static bool isTest=true;
		public static int imageNumber;

		public static View[] tempImages;
		public static int[] SavedImageStates;
		public static bool Once =true;
		public static bool advertiseOnce = true;
		public static Typeface font;
		public static View advert ;
	//	public static View tap_advert;
		public static GridView gv;
		public static TableLayout menu;
        public static ScrollView sv;
		public static TextView txt;
		public static TextView txt2;
		public static Button help;
		public static ImageView imgView;
		public static Dialog dialogX;
		public static int packageIndex=0;


		//Main Variables of UI ELEMENTS SIZE...
		public static int word = 10; 		//Empty Words to Fill (Size of UI)
		public static int guesWords = 12;	//Randomze Words to choose from (Size of UI)
		public static int imgtable = 46;	//Image Table Size ETC in HEight (Size of UI)
		public static int img = 23;		
		public static int imgw = 23;//Single Image Size in Height (Size of UI Image)
		public static int wordfont = 5;		//font Size in Game (Size of Fonts)
		public static int smallfont = 5;	
		public static int header =8;		//Header Size (Size of UI)
		public static int ad = 10;			//Size of ad (Size of ad)
		public static int guesWordTable = 18;		//Size of Image Table Size (Size of UIs)
		public static int wordTable = 8;		//Size of word Table size (Size of Ui)


		//>>>>>>>>>>>>>>>>>>>>>>>>>>
		public static bool isFull =false;		
		public struct Alphabet{
			public int alphabetBtnId;
			public char val;
		};
		public static Alphabet[] Alphabets;
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>

		/// <summary>
		/// Initiates the menu layout.
		/// </summary>
		public static void InitiateMenuLayout ()
		{
			HideLevelItems(true);
			GameManager.currentGameState = GameState.Menu;
			advert= activityLayout.FindViewById<View>(Resource.Id.mmadview);
			layout.RemoveView(advert);
		//	tap_advert= activityLayout.FindViewById<View>(Resource.Id.ad_view);
	//		layout.RemoveView(tap_advert);

			imgView.SetBackgroundResource(Resource.Drawable.WhatsThatWordHeader);
			menu = new TableLayout(activityLayout);

			menu.SetGravity (GravityFlags.Center);

            sv = new ScrollView(Activity1.MainlayoutActivity);
            sv.SmoothScrollingEnabled = (true);


			int[] btnNormal={
				Resource.Drawable.Leaderboards_normal,
				Resource.Drawable.CheckIn,
				Resource.Drawable.Orignal_normal,
				Resource.Drawable.Single_normal,
				Resource.Drawable.Double_normal,
				Resource.Drawable.Logos_normal,
                Resource.Drawable.Flags_normal,
                Resource.Drawable.Cars_normal,
                Resource.Drawable.Cartoon_normal,
				Resource.Drawable.Help_normal
			};

			int[] btnPressed={
				Resource.Drawable.Leaderboards_pressed,
				Resource.Drawable.CheckIn,
				Resource.Drawable.Orignal_pressed,
				Resource.Drawable.Single_pressed,
				Resource.Drawable.Double_Pressed,
				Resource.Drawable.Logos_pressed,
                Resource.Drawable.Flags_pressed,
                Resource.Drawable.Cars_pressed,
                Resource.Drawable.Cartoon_pressed,
				Resource.Drawable.Help_pressed

			};
			int cnt=0;
			for (int y =0; y<5; y++) {
				TableRow tr = new TableRow (activityLayout);
			
				tr.SetGravity(GravityFlags.Center);

				imgw=23;
				menu.AddView (tr, new TableRow.LayoutParams (
				TableRow.LayoutParams.FillParent, TableRow.LayoutParams.FillParent));
				int numOfColums=0;
			
				if(y==4)
				{
					numOfColums=2;
				}
				else
				{
					numOfColums=2;
				}


				for(int z=0;z<numOfColums;z++)
				{
					var imgW = SetUIWidthInPercent(45);
					var imgH = SetUIHeightInPercent(14);
					TableRow.LayoutParams p = new TableRow.LayoutParams (imgW,imgH );
					Button b = new Button (Activity1.MainlayoutActivity);
					b.Id=140+cnt;
				
					Selector(b,btnNormal[cnt],btnPressed[cnt],Resource.Drawable.button_b_disabled);
					b.Click+=(sender, e) => {

					if(b.Id==140)
						HeyZap.LoadLeaderBoard(activityLayout,"1VR");
					else if(b.Id==141)
							HeyZap.CheckIn(activityLayout);
					else if(b.Id==149)
							ViewHelp();
					else{
					switch(b.Id)
					{
		
					case 142:GameManager.currentLevelType=LevelType.Orignal;break;
					case 143:GameManager.currentLevelType=LevelType.Single;break;
					case 144:GameManager.currentLevelType=LevelType.Double;break;
					case 145:GameManager.currentLevelType=LevelType.Logos;break;
                    case 146: GameManager.currentLevelType = LevelType.Flags; break;
                    case 147: GameManager.currentLevelType = LevelType.Cars; break;
                    case 148: GameManager.currentLevelType = LevelType.Cartoons; break;
		
					}
					
					ShowPackageList((int)(GameManager.assets.List(@"Levels/"+GameManager.currentLevelType.ToString()).Length));
								}
				};
					cnt++;
				tr.AddView (b, p);
				}
			}
            //scrollview added by Arshad.
            //sv.AddView(menu, new LinearLayout.LayoutParams(
            //    LinearLayout.LayoutParams.FillParent, SetUIHeightInPercent(imgtable + guesWordTable + wordTable - 2)));

            //commented by Arshad115.
            layout.AddView(menu, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FillParent, SetUIHeightInPercent(imgtable + guesWordTable + wordTable - 2)));
            //layout.AddView(sv);
			RemoveLayoutExtraPaddingsAfterBackground(menu,Resource.Drawable.button_b_disabled);
	//		layout.AddView(tap_advert,new LinearLayout.LayoutParams(
	//			LinearLayout.LayoutParams.FillParent,LinearLayout.LayoutParams.FillParent));
			layout.AddView(advert,new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FillParent,LinearLayout.LayoutParams.FillParent));


		}
		/// <summary>
		/// Initiates the level selection layout.
		/// </summary>
		public static void InitiateLevelSelectionLayout (List<Levels> lvls, LevelType ltype)
		{
			HideLevelItems(true);
		
			switch (ltype) {
			case LevelType.Orignal:
				imgView.SetBackgroundResource(Resource.Drawable.Orignals);
				break;
			case LevelType.Single:
				imgView.SetBackgroundResource(Resource.Drawable.Singles);
				break;
			case LevelType.Double:
				imgView.SetBackgroundResource(Resource.Drawable.Doubles);
				break;
			case LevelType.Logos: 
				imgView.SetBackgroundResource(Resource.Drawable.Logos);
				break;
            case LevelType.Flags:
                imgView.SetBackgroundResource(Resource.Drawable.Flags);
                break;
            case LevelType.Cars:
                imgView.SetBackgroundResource(Resource.Drawable.Cars);
                break;
            case LevelType.Cartoons:
                imgView.SetBackgroundResource(Resource.Drawable.Cartoons);
                break;
			}
			//RemoveLayoutExtraPaddingsAfterBackground(activityLayout.FindViewById<RelativeLayout> (Resource.Id.view1),Resource.Drawable.header);
			GameManager.currentGameState=GameState.Selection;
	//		tap_advert= activityLayout.FindViewById<View>(Resource.Id.ad_view);
	//		layout.RemoveView(tap_advert);
			advert= activityLayout.FindViewById<View>(Resource.Id.mmadview);
			layout.RemoveView(advert);
			gv = new GridView(activityLayout);
			gv.SetColumnWidth(40);
			gv.SetNumColumns(3);
			gv.SetVerticalSpacing(5);
			gv.SetHorizontalSpacing(5);
			gv.StretchMode=StretchMode.StretchColumnWidth;
			gv.SetGravity(GravityFlags.Center);

			gv.Adapter=new ProductListAdapter(activityLayout,lvls,ltype);

			gv.ItemClick+=(sender, e) => {
			//	GameManager.currentGameState=GameState.Level;
				Console.WriteLine(e.Position+ " " + ltype.ToString());
				DeployLevelLayout(GameManager.levelList[e.Position],false,ltype);
			};

            //layout.RemoveView(sv);

			layout.AddView(gv,new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FillParent,SetUIHeightInPercent(imgtable+guesWordTable+wordTable-2)));
			layout.AddView(advert,new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FillParent,LinearLayout.LayoutParams.FillParent));
	//		layout.AddView(tap_advert,new LinearLayout.LayoutParams(
	//			LinearLayout.LayoutParams.FillParent,LinearLayout.LayoutParams.FillParent));

		}


		/// <summary>
		/// Initiates the level layout.
		/// </summary>
		public static void InitiateLevelLayout ()
		{

		//	RelativeLayout view = activityLayout.FindViewById<RelativeLayout> (Resource.Id.view1);
		//	view .LayoutParameters=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FillParent,
			                                     //                SetUIHeightInPercent(header));
			//RemoveLayoutExtraPaddingsAfterBackground(view,Resource.Drawable.header);
		//	view.SetBackgroundResource(Resource.Drawable.header);
			HideLevelItems(false);
	//		tap_advert= activityLayout.FindViewById<View>(Resource.Id.ad_view);
	//		layout.RemoveView(tap_advert);
			advert= activityLayout.FindViewById<View>(Resource.Id.mmadview);
			layout.RemoveView(advert);

            wordButtonLayout = new TableLayout (activityLayout);

			wordButtonLayout.Id = 444;

			wordlayout = new TableLayout (activityLayout);

			RemoveLayoutExtraPaddingsAfterBackground(wordButtonLayout,Resource.Drawable.button_b_disabled);

			imgTable = new TableLayout (activityLayout);
			imgTable.Id = 88;
			RemoveLayoutExtraPaddingsAfterBackground(imgTable,Resource.Drawable.button_b_disabled);
			wordlayout.SetGravity (GravityFlags.Center);
			wordButtonLayout.SetGravity (GravityFlags.Center);
			imgTable.SetClipChildren (false);
			imgTable.SetClipToPadding (false);

           

			layout.AddView (imgTable, new TableLayout.LayoutParams (
				TableLayout.LayoutParams.FillParent,SetUIHeightInPercent(imgtable)));
			layout.AddView (wordlayout, new TableLayout.LayoutParams (
				TableLayout.LayoutParams.FillParent, SetUIHeightInPercent (wordTable)));
			layout.AddView (wordButtonLayout, new TableLayout.LayoutParams (
				TableLayout.LayoutParams.FillParent, SetUIHeightInPercent (guesWordTable)));
			layout.AddView (advert, new LinearLayout.LayoutParams (
				LinearLayout.LayoutParams.FillParent,LinearLayout.LayoutParams.FillParent));

	//		layout.AddView (tap_advert, new LinearLayout.LayoutParams (
	//			LinearLayout.LayoutParams.FillParent,LinearLayout.LayoutParams.FillParent));

		}

		/// <summary>
		/// Deploies the layout.
		/// </summary>
		/// <param name='level'>
		/// Level.
		/// </param>
		public static void DeployLevelLayout (Levels level,bool isInitiate,LevelType lvlType)
		{

			GameManager.currentGameState=GameState.Level;
			GameManager.currentLevel = level;
			tempImages = new View[4];
			SavedImageStates = new int[4];
			spellCount = 0;
			GameManager.revealLetterCount = 0;
			GameManager.removeLetterCount = 0;
			if (!isInitiate) {
				layout.RemoveView(gv);
				gv=null;
				switch(lvlType)
				{
				case LevelType.Orignal:
					imgtable=46;img=23;imgw=46;
					word=11;
					wordTable =8;break;
				case LevelType.Double:
					imgtable=40;img=48;imgw=48;word=11;
					wordTable=16;break;
				case LevelType.Single:
                case LevelType.Flags:
                case LevelType.Cars: 
				case LevelType.Logos:
                case LevelType.Cartoons:
					imgtable=46;img=55;imgw=55;word=11;
					wordTable=8;break;

				}

				InitiateLevelLayout();
			}
			string value = level.word;
			string[] vals = new string[2];
			switch (lvlType) {
			case LevelType.Orignal:
			case LevelType.Single:
            case LevelType.Flags:
            case LevelType.Cars:
			case LevelType.Logos:
            case LevelType.Cartoons:
				vals[0]=value;
				vals[1]=string.Empty;
				//row=1;
				break;
			case LevelType.Double:
				vals=value.Split('_');
				//row=2;
				break;
				
			}


			currentLevelAnswer =(vals[0]+vals[1]);
			Alphabets = new Alphabet[(vals[0]+vals[1]).Length];

			for (int y =0; y<(vals[0]+vals[1]).Length; y++)
				Alphabets [y].val = ' ';

				GameManager.UpdateStatus();
		
			CreateImageButton (GameManager.GetImagesNameFromFolder (level.word,lvlType),lvlType);
			GenerateEmptyButtons (value,lvlType);
			GenerateButtonsArray (value,lvlType);
								

		}
		/// <summary>
		/// Removes the layout extra paddings after background.
		/// </summary>
		/// <param name='v'>
		/// V.
		/// </param>
		/// <param name='drawableID'>
		/// Drawable I.
		/// </param>
		public static void RemoveLayoutExtraPaddingsAfterBackground (View v,int drawableID)
		{
			int bottom1 = v.Bottom;
			int top1 = v.Top;
			int right1 = v.Right;
			int left1 = v.Left;
			v.SetBackgroundResource (drawableID);
			v.SetPadding (left1, top1, right1, bottom1);
		}
         
		public static void ShowPackageList (int count)
		{
			dialogX = new Dialog (activityLayout);
			
			dialogX.SetContentView (Resource.Layout.PopupList);// .setContentView(R.layout.custom);
			dialogX.SetTitle ("Select Level Package");
			dialogX.Show ();

			ListView lv = dialogX.FindViewById(Resource.Id.packageList) as ListView;
			List<string> btns = new List<string>();
			for(int x=0;x<count;x++)
				btns.Add("Level Package "+(x+1));
			lv.Adapter=new PackageList(activityLayout,btns);
		
		}

		/// <summary>
		/// Shows the dialog.
		/// </summary>
		/// <param name='id'>
		/// Identifier.
		/// </param>
		public static void ShowDialog (int id)
		{
			// custom dialog
			if (id == 0) {
				Dialog dialog = new Dialog (activityLayout);

				dialog.SetContentView (Resource.Layout.HelpDialog);// .setContentView(R.layout.custom);
				dialog.SetTitle ("Use Points");
				dialog.Show ();

			
				Button dialogButton = (Button)dialog.FindViewById (Resource.Id.item1);//(Button) dialog.findViewById(R.id.dialogButtonOK);
				// if button is clicked, close the custom dialog
				dialogButton.Click += delegate {
			

					dialog.Dismiss ();
					dialog.Dispose ();
					dialog = null;
					try {
						GameManager.UsePoints (LevelHelp.reveal_letter);
					} catch (System.Exception e) {
						MessageBox (e.ToString ());
					}
				};
				Button dialogButton1 = (Button)dialog.FindViewById (Resource.Id.item2);//(Button) dialog.findViewById(R.id.dialogButtonOK);
				// if button is clicked, close the custom dialog
				dialogButton1.Click += delegate {


					dialog.Dismiss ();
					dialog.Dispose ();
					dialog = null;
					try {
						GameManager.UsePoints (LevelHelp.remove_letter);
					} catch (System.Exception e) {
						MessageBox (e.ToString ());
					}
				};

				Button dialogButton3 = (Button)dialog.FindViewById (Resource.Id.buyPoints);//(Button) dialog.findViewById(R.id.dialogButtonOK);
				// if button is clicked, close the custom dialog
				dialogButton3.Click += delegate {

						dialog.Dismiss();
						dialog.Dispose();
						dialog=null;
					ShowDialog(1);
				};
                 
				Button dialogButton4 = (Button)dialog.FindViewById (Resource.Id.cancel);//(Button) dialog.findViewById(R.id.dialogButtonOK);
				// if button is clicked, close the custom dialog
				dialogButton4.Click += delegate {
				
					dialog.Dismiss ();
					dialog.Dispose ();
					dialog = null;
				};

				Button dialogButton5 = (Button)dialog.FindViewById (Resource.Id.Share);//(Button) dialog.findViewById(R.id.dialogButtonOK);
				// if button is clicked, close the custom dialog
				if(GameManager.isGotSharedPoints())
				{

					dialogButton5.SetText("Share (Get Free 100 POINTS)",TextView.BufferType.Normal);
				}
				else{
					dialogButton5.SetText("Share",TextView.BufferType.Normal);
				}

				dialogButton5.Click += delegate {
					//create the send intent
					if(GameManager.isGotSharedPoints())
					{
						GameManager.points+=100;
						GameManager.UpdateStatus();
						ISharedPreferencesEditor editor1 =Android.Preferences.PreferenceManager.GetDefaultSharedPreferences(LayoutManager.activityLayout).Edit ();
						
						editor1.PutString ("share_again", "false");
						editor1.Commit ();
					}

					Intent shareIntent = 
						new Intent(Intent.ActionSend);
					
					//set the type
					shareIntent.SetType("text/plain");
					
					//add a subject
					shareIntent.PutExtra(Intent.ExtraSubject, 
					                     "Check this out");
					
					//build the body of the message to be shared
					string shareMessage = "Hey, check out what's that word game on android (https://play.google.com/store/apps/details?id=com.whatsthatword.game&feature=search_result)";
					
					//add the message
					shareIntent.PutExtra(Intent.ExtraText, 
					                     shareMessage);
					
					//start the chooser for sharing
					activityLayout.StartActivity(Intent.CreateChooser(shareIntent,"Share Whats that word with others"));
					//StartActivity(Intent.createChooser(shareIntent, 
					  //                                 "Insert share chooser title here"));
					dialog.Dismiss ();
					dialog.Dispose ();
					dialog = null;
				};

			} else if (id == 1) {
				Dialog dialog = new Dialog (activityLayout);
				dialog.SetContentView (Resource.Layout.BuyArea);// .setContentView(R.layout.custom);
				dialog.SetTitle ("Buy Points");
				dialog.Show ();
				
				
				Button dialogButton = (Button)dialog.FindViewById (Resource.Id.buyItem1);//(Button) dialog.findViewById(R.id.dialogButtonOK);
				// if button is clicked, close the custom dialog
				dialogButton.Click += delegate {
					
					
					dialog.Dismiss ();
					dialog.Dispose ();
					dialog = null;
					GameManager.productID = 1;
					GameManager.purchaser.InAppBilling("coin_item_1");

				};
				Button dialogButton1 = (Button)dialog.FindViewById (Resource.Id.buyItem2);//(Button) dialog.findViewById(R.id.dialogButtonOK);
				// if button is clicked, close the custom dialog
				dialogButton1.Click += delegate {
					
					
					dialog.Dismiss ();
					dialog.Dispose ();
					dialog = null;
					GameManager.productID = 2;
					GameManager.purchaser.InAppBilling("coin_item_2");

				};
				Button dialogButton2 = (Button)dialog.FindViewById (Resource.Id.buyItem3);//(Button) dialog.findViewById(R.id.dialogButtonOK);
				// if button is clicked, close the custom dialog
				dialogButton2.Click += delegate {
					
					
					dialog.Dismiss ();
					dialog.Dispose ();
					dialog = null;
					GameManager.productID = 3;
					GameManager.purchaser.InAppBilling("coin_item_3");

				};
				Button dialogButton3 = (Button)dialog.FindViewById (Resource.Id.buyItem4);//(Button) dialog.findViewById(R.id.dialogButtonOK);
				// if button is clicked, close the custom dialog
				dialogButton3.Click += delegate {
					
						dialog.Dismiss();
						dialog.Dispose();
						dialog=null;
					GameManager.productID = 4;
					GameManager.purchaser.InAppBilling("coin_item_4");
				};
				
				Button dialogButton4 = (Button)dialog.FindViewById (Resource.Id.cancel1);//(Button) dialog.findViewById(R.id.dialogButtonOK);
				// if button is clicked, close the custom dialog
				dialogButton4.Click += delegate {
					
					dialog.Dismiss ();
					dialog.Dispose ();
					dialog = null;
				};
			}

		}
		/// <summary>
		/// Sets the user interface height in percent.
		/// </summary>
		/// <returns>
		/// The user interface height in percent.
		/// </returns>
		/// <param name='percent'>
		/// Percent.
		/// </param>
		public static int SetUIHeightInPercent (int percent)
		{
			var metrics = activityLayout.Resources.DisplayMetrics;

			double val = (double)percent/100;


			Android.Content.Context con = Application.Context;//new Android.Content.Context();
			Android.Views.IWindowManager win =con.GetSystemService(Context.WindowService).JavaCast<IWindowManager>(); 
		
			Display d = win.DefaultDisplay;
			var heightInDp = d.Height;

			return (int)(heightInDp*val);
		}
		/// <summary>
		/// Sets the user interface width in percent.
		/// </summary>
		/// <returns>
		/// The user interface width in percent.
		/// </returns>
		/// <param name='percent'>
		/// Percent.
		/// </param>
		public static int SetUIWidthInPercent (int percent)
		{
			var metrics = activityLayout.Resources.DisplayMetrics;
			//percent=40;
			double val = (double)percent/100;
			Android.Content.Context con = Application.Context;//new Android.Content.Context();
			Android.Views.IWindowManager win =con.GetSystemService(Context.WindowService).JavaCast<IWindowManager>(); 
			//	var widthInDp = ConvertPixelsToDp(metrics.WidthPixels);
			Display d = win.DefaultDisplay;// .DefaultDisplay();
		//	var heightInDp = d.Height; //ConvertPixelsToDp(metrics.HeightPixels);
		
			var widthInDp =d.Width;// ConvertPixelsToDp(metrics.WidthPixels);
			
			//var heightInDp = ConvertPixelsToDp(metrics.HeightPixels);
			//	Console.WriteLine(heightInDp + " " + metrics.HeightPixels + " " + (int)(heightInDp*val));
			return (int)(widthInDp*val);
		}

		/// <summary>
		/// Creates the image button.
		/// </summary>
		/// <param name='imgBit'>
		/// Image bit.
		/// </param>
		private static void CreateImageButton (Bitmap[] imgBit, LevelType lType)
		{
			int cnt = 0;
			int rowCount = 0;
			int imgCount = 0;

			switch (lType) {
			case LevelType.Orignal:rowCount=2;imgCount=2;break;
			case LevelType.Double:rowCount=1;imgCount=2;break;
			case LevelType.Single:
            case LevelType.Flags:
            case LevelType.Cars:
			case LevelType.Logos:
            case LevelType.Cartoons:
				rowCount=1;imgCount=1;break;
			}
			
			for (int y =0; y<rowCount; y++) {
				TableRow tr = new TableRow (activityLayout);

				imgTable.AddView (tr, new TableRow.LayoutParams (
					TableRow.LayoutParams.FillParent, TableRow.LayoutParams.FillParent
					));
				var imgW = SetUIWidthInPercent(imgw);
				var imgH = SetUIHeightInPercent(img);
				tr.SetGravity(GravityFlags.Center);
			
				for (int x=0; x<imgCount; x++) {
					TableRow.LayoutParams p = new TableRow.LayoutParams (imgW,
					                                                    imgH);
					ImageView b = new ImageView(activityLayout);
					b.Id= 150+cnt;
					tempImages[cnt]=b;

					b.SetImageBitmap(imgBit[cnt++]);

					if(lType==LevelType.Orignal)
						b.Enabled=true;
					else
						b.Enabled=false;

					b.Click+=delegate {
					
						if(isTest)
						{
							isTest=false;

							switch(b.Id)
							{
							case 150:replace(b,0,0,0,0,1,2,1,2);
								imageNumber = 0;
								SavedImageStates[0]=0;
								SavedImageStates[1]=0;
								SavedImageStates[2]=0;
								SavedImageStates[3]=0;
								tr.BringToFront();
								tr.BringChildToFront(tempImages[imageNumber]);
								break;
							case 151:replace(b,0,-(imgW),0,0,1,2,1,2);
								imageNumber=1;
								SavedImageStates[0]=-(imgW);
								SavedImageStates[1]=0;
								SavedImageStates[2]=0;
								SavedImageStates[3]=0;
								tr.BringToFront();
								tr.BringChildToFront(tempImages[imageNumber]);
								break;
							case 152:replace(b,0,0,0,-(imgH),1,2,1,2);
								imageNumber=2;
								SavedImageStates[0]=0;
								SavedImageStates[1]=0;
								SavedImageStates[2]=-(imgH);
								SavedImageStates[3]=0;
								tr.BringToFront();
								tr.BringChildToFront(tempImages[imageNumber]);
								break;
							case 153:replace(b,0,-(imgW),0,-(imgH),1,2,1,2);
								imageNumber=3;
								SavedImageStates[0]=-(imgW);
								SavedImageStates[1]=0;
								SavedImageStates[2]=-(imgH);
								SavedImageStates[3]=0;
								tr.BringToFront();
								tr.BringChildToFront(tempImages[imageNumber]);
								break;
							}

						} 
						else{
							isTest=true;
							replace(tempImages[imageNumber],SavedImageStates[0],
							        SavedImageStates[1],
							       	SavedImageStates[2],
							        SavedImageStates[3],
							        2,1,2,1);
							tempImages[imageNumber].Invalidate();


						}
					};
					
					tr.AddView (b, p);
				}

			}
		//	layout.AddView (imgTable, new TableLayout.LayoutParams (
		//		TableLayout.LayoutParams.FillParent,SetUIHeightInPercent(imgtable)));
		}
		/// <summary>
		/// Replace the specified v, xFrom, xTo, yFrom, yTo, xSFrom, xScale, ySFrom and yScale.
		/// </summary>
		/// <param name='v'>
		/// V.
		/// </param>
		/// <param name='xFrom'>
		/// X from.
		/// </param>
		/// <param name='xTo'>
		/// X to.
		/// </param>
		/// <param name='yFrom'>
		/// Y from.
		/// </param>
		/// <param name='yTo'>
		/// Y to.
		/// </param>
		/// <param name='xSFrom'>
		/// X S from.
		/// </param>
		/// <param name='xScale'>
		/// X scale.
		/// </param>
		/// <param name='ySFrom'>
		/// Y S from.
		/// </param>
		/// <param name='yScale'>
		/// Y scale.
		/// </param>
		public static void replace(View v,int xFrom,int xTo,int yFrom, int yTo,float xSFrom, float xScale,float ySFrom, float yScale) {
			// create set of animations
			AnimationSet replaceAnimation = new AnimationSet(false);

			replaceAnimation.FillAfter=true;

			// create scale animation
			ScaleAnimation scale = new ScaleAnimation(xSFrom, xScale, ySFrom, yScale);
			scale.Duration=500;// .setDuration(1000);


			// create translation animation
			TranslateAnimation trans = new TranslateAnimation(
				xFrom,xTo,yFrom,yTo
				);
			trans.Duration=500;// .setDuration(1000);
			
			// add new animations to the set
			replaceAnimation.AddAnimation(scale);// .addAnimation(scale);
			replaceAnimation.AddAnimation(trans);// .addAnimation(trans);
			
			// start our animation
			v.StartAnimation(replaceAnimation);
			//startAnimation(replaceAnimation);
		}
		/// <summary>
		/// Clears the layout.
		/// </summary>
		public static void ClearLayout()
		{
			wordButtonLayout.RemoveAllViews();
			wordlayout.RemoveAllViews();
			imgTable.RemoveAllViews();
		
		}

		public static void ClearLevelLayout()
		{
			layout.RemoveView(wordButtonLayout);
			
			layout.RemoveView(wordlayout);
			
			layout.RemoveView(imgTable);

			wordButtonLayout=null;
			wordlayout=null;
			imgTable=null;
			
		}

		public static void ClearSelectionLayout()
		{
			layout.RemoveView(gv);
			gv=null;
			
		}

		public static void InitiateHeader ()
		{
			txt = new TextView(activityLayout);
			txt.Id=754;//activityLayout.FindViewById<TextView> (Resource.Id.level);
			txt2 = new TextView(activityLayout);
			txt2.Id=757;
			imgView = new ImageView(activityLayout);
			//imgView.SetBackgroundResource(Resource.Drawable.WhatsThatWordHeader);
			help = new Button(activityLayout);//activityLayout.FindViewById<TextView> (Resource.Id.point);
			RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WrapContent,RelativeLayout.LayoutParams.FillParent);
			RelativeLayout.LayoutParams rlp1 = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WrapContent,RelativeLayout.LayoutParams.FillParent);
			RelativeLayout.LayoutParams rlp2 = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WrapContent,RelativeLayout.LayoutParams.FillParent);
			RelativeLayout view = activityLayout.FindViewById<RelativeLayout> (Resource.Id.view1);
			view .LayoutParameters=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FillParent,
			                                                     SetUIHeightInPercent(header));
			RemoveLayoutExtraPaddingsAfterBackground(view,Resource.Drawable.header);
			view.AddView(txt,rlp);
			rlp1.AddRule(LayoutRules.AlignParentRight);
			view.AddView(txt2,rlp1);

			rlp2.AddRule(LayoutRules.CenterInParent);
			view.AddView(imgView,new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.FillParent,RelativeLayout.LayoutParams.FillParent));
			view.AddView(help,rlp2);


			var fontval = SetUIWidthInPercent(smallfont);
			txt.SetBackgroundResource(Resource.Drawable.button1_gallery);
			txt.SetTypeface(font,TypefaceStyle.Bold);
			txt2.SetBackgroundResource(Resource.Drawable.button1_gallery);
			txt2.SetTypeface(font,TypefaceStyle.Bold);
			txt.SetTextSize (Android.Util.ComplexUnitType.Px, fontval);
			txt2.SetTextSize (Android.Util.ComplexUnitType.Px, fontval);

			//activityLayout.FindViewById<Button>(Resource.Id.help);
			help.SetTextSize (Android.Util.ComplexUnitType.Px,fontval);
			var bbw = SetUIHeightInPercent(10);
			help.SetWidth(bbw);
			help.SetTextColor(Color.White);
			help.SetText("?",TextView.BufferType.Normal);
			help.SetTypeface(font,TypefaceStyle.Bold);
			Selector (help, Resource.Drawable.button_r,
			          Resource.Drawable.button_b,
			          Resource.Drawable.button_b_disabled);
			help.Click += delegate {
				ShowDialog (0);
				
			};
			view.Invalidate();

		}


	
		/// <summary>
		/// Generates the buttons array.
		/// </summary>
		/// <param name='word'>
		/// Word.
		/// </param>
		public static void GenerateButtonsArray (string word,LevelType lType)
		{

			//int cnt=0;
			int numBtn=12;
			string[] vals = new string[2];
			switch (lType) {
			case LevelType.Orignal:
			case LevelType.Single:
            case LevelType.Flags:
            case LevelType.Cars:
			case LevelType.Logos:
            case LevelType.Cartoons:
				vals[0]=word;
				vals[1]=string.Empty;
				numBtn=12;
				break;
			case LevelType.Double:
				vals=word.Split('_');
				numBtn=16;
				break;
				
			}

			char[] val =RandomizeCharacters(
				WordProcessor.GenerateRandomCharArrayForButtons ((vals[0]+vals[1]).ToUpper (),numBtn)
				);
			val = RandomizeCharacters(val);
			char[] filteredChar = WordProcessor.FilterMainString(new string(val),(vals[0]+vals[1]).ToUpper());
		
			int cnt=0;
			ExtraButtons = new int[numBtn -(vals[0]+vals[1]).Length];
		
			var Button_W = (int)(SetUIWidthInPercent(guesWords));
			int halfnum=numBtn/2;
			for (int y =0; y<2; y++) {
				TableRow tr = new TableRow (activityLayout);
				tr.SetClipChildren(false);
				tr.SetClipToPadding(false);
				tr.SetGravity(GravityFlags.Center);
				wordButtonLayout.AddView (tr, new TableRow.LayoutParams (
				TableRow.LayoutParams.FillParent, Button_W));

				for (int x=0; x<halfnum; x++) {
					TableRow.LayoutParams p = new TableRow.LayoutParams (Button_W,Button_W );
					Button b = new Button (Activity1.MainlayoutActivity);
					//Console.WriteLine(wordButtonLayout.Width);
					b.SetText (val, x+y*halfnum, 1);
					b.Id = 100 +  x+y*halfnum;

					if(filteredChar[x+y*halfnum]!='@')
						ExtraButtons[cnt++]=b.Id;


					b.SetTextColor(Color.White);
					var fontval = SetUIWidthInPercent(wordfont);
					b.SetTextSize(Android.Util.ComplexUnitType.Px, fontval);
					b.SetTypeface(font,TypefaceStyle.Bold);
					Selector(b,Resource.Drawable.button1,Resource.Drawable.button1_pressed,Resource.Drawable.button1_disabled);
					b.Click+=delegate {
						if(spellCount<(vals[0]+vals[1]).Length)
						{

							Button btn = activityLayout.FindViewById<Button>(200+GetEmptyBoxIndex((vals[0]+vals[1])));
							Alphabets[btn.Id-200].alphabetBtnId = b.Id;
							Alphabets[btn.Id-200].val=b.Text.ToCharArray()[0];
							SwapViews(b,btn,true);
							spellCount++;
							if(spellCount==(vals[0]+vals[1]).Length)
								ShowAlert(IsCorrect((vals[0]+vals[1])));
						}
					};

					tr.AddView (b, p);
				}

			}
		}



		/// <summary>
		/// Selector the specified b, buttonID, buttonID2 and buttonID3.
		/// </summary>
		/// <param name='b'>
		/// The blue component.
		/// </param>
		/// <param name='buttonID'>
		/// Button I.
		/// </param>
		/// <param name='buttonID2'>
		/// Button I d2.
		/// </param>
		/// <param name='buttonID3'>
		/// Button I d3.
		/// </param>
		public static void Selector(Button b,int buttonID,int buttonID2 ,int buttonID3)
		{
			StateListDrawable states = new StateListDrawable();
			states.AddState(new int[] {Android.Resource.Attribute.StatePressed}, activityLayout.Resources.GetDrawable(buttonID2));  
			states.AddState(new int[] {Android.Resource.Attribute.StateEnabled},activityLayout.Resources.GetDrawable(buttonID));    
			states.AddState(new int[] { },activityLayout.Resources.GetDrawable(buttonID3));      

			b.SetBackgroundDrawable(states);
		}
		/// <summary>
		/// Determines if is correct the specified word.
		/// </summary>
		/// <returns>
		/// <c>true</c> if is correct the specified word; otherwise, <c>false</c>.
		/// </returns>
		/// <param name='word'>
		/// Word.
		/// </param>
		public static bool IsCorrect (string word)
		{
			char[] values = new char[word.Length];
			for(int z=0;z<word.Length;z++)
				values[z]= Alphabets[z].val;

			if(WordProcessor.IsEqual(new string(values).Trim(),word.ToUpper()))
				return true;
			else
				return false;

		}
		/// <summary>
		/// Determines if is button identifier used the specified id.
		/// </summary>
		/// <returns>
		/// <c>true</c> if is button identifier used the specified id; otherwise, <c>false</c>.
		/// </returns>
		/// <param name='id'>
		/// Identifier.
		/// </param>
		public static bool IsButtonIdUsed (int id)
		{
			for(int u=0;u<Alphabets.Length;u++)
				if(Alphabets[u].alphabetBtnId==id)
					return false;

			return true;
		}

		public static void ViewHelp()
		{
			Dialog dialog = new Dialog (activityLayout);
			
			dialog.SetContentView (Resource.Layout.PopUpHelp);// .setContentView(R.layout.custom);
		
			TextView txt = dialog.FindViewById (Resource.Id.helpresult) as TextView;
			txt.SetText("Thank You For Downloading What's That Word!\n" +
			            "How To Play:\n" +
			            "Find the word that relates to the pictures in each level!\n" +
			            "In DOUBLE you have to find two words that relate to the pictures to move to the next level!\n" +
			            "Find the similarity in each picture!\n" +
			            "Example:\n" +
			            "If you see four pictures with a tooth brush, hair brush, paint brush, and make up brush. The \n" +
			            "word we're looking for is BRUSH! Thats a easy one. It does get tricky. Good luck\n" +
			            "Credits:\n" +
			            "Images provided by http://www.jupiterimages.com\n" +
			            "Fonts Provided by http://www8.flamingtext.com" +
			            "What's That Word brought to you by http://www.MoPlays.com\n" +
			            "Send suggestions and things you would like to see in app to moplays1@gmail.com",TextView.BufferType.Normal);
			dialog.Show();
		}

		/// <summary>
		/// Shows the alert.
		/// </summary>
		/// <param name='isWon'>
		/// If set to <c>true</c> is won.
		/// </param>
		public static void ShowAlert (bool isWon)
		{
			Dialog dialog = new Dialog (activityLayout);
			
			dialog.SetContentView (Resource.Layout.Popup);

			TextView txt = (TextView)dialog.FindViewById (Resource.Id.result);

			bool isGotScore=false;

			if (isWon) {

				txt.SetTextColor (Color.Green);
				txt.SetTypeface (font, TypefaceStyle.Bold);

				txt.SetTextSize (Android.Util.ComplexUnitType.Px, SetUIWidthInPercent (7));
				dialog.Show ();
				//	if(GameManager.levelCounter==GameManager.levelList.Count)
				//		GameManager.levelCounter=0;
				//	if()
				if (!GameManager.IsCompletedBefore (GameManager.currentLevel.word)) {
					GameManager.points += 10;
					GameManager.levelsDone.Add (GameManager.currentLevel.word);
					GameManager.UpdateStatus ();
					txt.SetText ("Awesome +10 Points", TextView.BufferType.Normal);
					GameManager.totalScore += 100;
					isGotScore=true;
				} else {
					txt.SetText ("RIGHT GUESS ! ", TextView.BufferType.Normal);
				}


				//	GameManager.NextLevel();
				new Handler ().PostDelayed (new Runnable (() =>
				{
					LayoutManager.ClearLevelLayout ();
					GameManager.GetAllLevel (GameManager.currentLevelType, LayoutManager.packageIndex);
					LayoutManager.InitiateLevelSelectionLayout (GameManager.levelList, GameManager.currentLevelType);
					dialog.Dismiss ();
					dialog.Dispose ();

					dialog = null;

				}), 1000);

			} else {
				txt.SetTextColor (Color.Red);
				txt.SetTypeface (font, TypefaceStyle.Bold);
				txt.SetText ("WRONG GUESS !", TextView.BufferType.Normal);
				txt.SetTextSize (Android.Util.ComplexUnitType.Px, SetUIWidthInPercent (7));
				dialog.Show ();
				
				new Handler ().PostDelayed (new Runnable (() =>
				{
					dialog.Dismiss ();
					dialog.Dispose ();
					dialog = null;
				}), 1000);
			}
			if (isGotScore) {
				HeyZap.PutScore (activityLayout,
			                GameManager.totalScore.ToString (),
			                GameManager.totalScore.ToString (),
			                "1VR");
			}

		}
		/// <summary>
		/// Messages the box.
		/// </summary>
		/// <param name='val'>
		/// Value.
		/// </param>
		public static void MessageBox (string val)
		{
			
			AlertDialog.Builder builder1 = new AlertDialog.Builder (activityLayout);
			builder1.SetMessage (val);
			builder1.SetNegativeButton ("ok", (sender,e) => { 

				builder1.Dispose (); 
				
			}); 
			builder1.Show(); 
		}
		/// <summary>
		/// Buies the point box.
		/// </summary>
		/// <param name='val'>
		/// Value.
		/// </param>
		public static void BuyPointBox (string val)
		{
			
			AlertDialog.Builder builder1 = new AlertDialog.Builder (activityLayout);
			builder1.SetMessage (val);
			builder1.SetPositiveButton ("Buy Points", (sender,e) => { 
				
				builder1.Dispose (); 
				ShowDialog(1);
			}); 
			builder1.SetNegativeButton ("Cancel", (sender,e) => { 
				
				builder1.Dispose (); 
				
			}); 
			builder1.Show(); 
		}
		/// <summary>
		/// Randomizes the characters.
		/// </summary>
		/// <returns>
		/// The characters.
		/// </returns>
		/// <param name='val'>
		/// Value.
		/// </param>
		public static char[] RandomizeCharacters(char[] val)
		{

			Random num = new Random();
			return new string(val.OrderBy(s => (num.Next(2) % 2) == 0).ToArray()).ToCharArray();
		}


		/// <summary>
		/// Animates the view to another view.
		/// </summary>
		/// <param name='v1'>
		/// V1.
		/// </param>
		/// <param name='v2'>
		/// V2.
		/// </param>
		/// <param name='duration'>
		/// Duration.
		/// </param>
		public static void AnimateViewToAnotherView (Button v1, Button v2,int duration)
		{
		
			int[] origPos = new int[2];
			int[] desPos = new int[2];
			v2.GetLocationOnScreen(origPos);
			v1.GetLocationOnScreen(desPos);
			TranslateAnimation mAnimation = new TranslateAnimation(
				0,origPos[0]-desPos[0],0,origPos[1]-desPos[1]
				);



			mAnimation.Duration=duration;
			mAnimation.FillAfter=true;
			mAnimation.RepeatCount=0;
			
			
		
			v1.Animation=mAnimation;


		}
	
	/*	public static void InitiateHeader()
		{
			Header = new TableLayout(activityLayout);
			layout.AddView(Header,new TableLayout.LayoutParams(
				TableLayout.LayoutParams.FillParent,
				40));
			TableRow tr = new TableRow(activityLayout);
			Header.AddView(tr,new TableRow.LayoutParams(
				TableRow.LayoutParams.FillParent,
				40)
			               );
			TextView txt1 = new TextView(activityLayout);
			tr.AddView(txt1,new TableRow.LayoutParams(
				TableRow.LayoutParams.FillParent/3,TableRow.LayoutParams.FillParent
				)
			           );

		}*/

		/// <summary>
		/// Swaps the views.
		/// </summary>
		/// <param name='v1'>
		/// V1.
		/// </param>
		/// <param name='v2'>
		/// V2.
		/// </param>
		/// <param name='val'>
		/// If set to <c>true</c> value.
		/// </param>
		public static void SwapViews (Button v1, Button v2, bool val)
		{
			if (val) {
				v2.SetText (v1.Text, TextView.BufferType.Normal);
				v1.Enabled=false;
				v2.Enabled = true;
				v1.Visibility = ViewStates.Invisible;
				v2.Invalidate ();
			} else {
				v1.Enabled=false;
				v2.Enabled=true;
				v1.SetText("",TextView.BufferType.Normal);
				v2.Visibility = ViewStates.Visible;
			}
		}

		public static void HideLevelItems (bool isTo)
		{
			if (isTo) {
				txt.Visibility = ViewStates.Gone;
			
				txt2.Visibility = ViewStates.Gone;
			
				help.Visibility = ViewStates.Gone;
				imgView.Visibility = ViewStates.Visible;
			} else {
				txt.Visibility = ViewStates.Visible;
				
				txt2.Visibility = ViewStates.Visible;
				
				help.Visibility = ViewStates.Visible;
				imgView.Visibility = ViewStates.Gone;
			}
		}

		/// <summary>
		/// Gets the empty index of the box.
		/// </summary>
		/// <returns>
		/// The empty box index.
		/// </returns>
		/// <param name='val'>
		/// Value.
		/// </param>
		public static int GetEmptyBoxIndex (string val)
		{

			for (int x=0; x<val.Length; x++) {
				Button btn = activityLayout.FindViewById<Button>(200+x);
				if(btn.Text.Equals(""))
				   return x;
			}
			return val.Length+1;
		}
		/// <summary>
		/// Generates the empty buttons.
		/// </summary>
		/// <param name='val'>
		/// Value.
		/// </param>
		public static void GenerateEmptyButtons (string val, LevelType lType)
		{
			int row = 0;
			int cnt=0;
			string[] vals = new string[1];
			switch (lType) {
			case LevelType.Orignal:
			case LevelType.Single:
            case LevelType.Flags:
            case LevelType.Cars:
			case LevelType.Logos:
            case LevelType.Cartoons:
				vals[0]=val;
				row=1;break;
			case LevelType.Double:
				vals=val.Split('_');
				row=2;break;
			
			}

			for (int y=0; y<row; y++) {
				TableRow tr = new TableRow (activityLayout);

				tr.SetClipChildren (false);
				tr.SetClipToPadding (false);
				var Button_W = SetUIWidthInPercent (word);
			
				tr.SetGravity (GravityFlags.Center);
		
				wordlayout.AddView (tr, new TableRow.LayoutParams (
				TableRow.LayoutParams.FillParent, Button_W
				));

				for (int x=0; x<vals[y].Length; x++) {
					LinearLayout.LayoutParams p = new TableRow.LayoutParams (Button_W, Button_W);
					Button b = new Button (Activity1.MainlayoutActivity);
			
					b.Id = 200 + cnt++;
					b.SetWidth (Button_W);
					var fontval = SetUIWidthInPercent (smallfont);
					b.SetTextSize (Android.Util.ComplexUnitType.Px, fontval);
					b.SetTextColor (Color.White);
					b.SetTypeface (font, TypefaceStyle.Bold);
					b.Enabled = false;
					Selector (b, Resource.Drawable.button1, Resource.Drawable.button1_pressed, Resource.Drawable.button1_disabled);
					b.Click += delegate {
						Button btn = activityLayout.FindViewById<Button> (Alphabets [b.Id - 200].alphabetBtnId);
						Alphabets [b.Id - 200].alphabetBtnId = 0;
						Alphabets [b.Id - 200].val = ' ';
						SwapViews (b, btn, false);
						spellCount--;
					};
					tr.AddView (b, p);
				}
			}
		}
	}
}

