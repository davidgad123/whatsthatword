using System;
using System.IO;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Preferences;
using Android.Content.Res;
using Android.Content.PM;
using Billing;
using Android.Util;
using Android.Database;
using Java.Lang;
using System.Collections.Generic;

namespace GuessTheWord
{
	[Activity (Label = "Whats That Word",
	           MainLauncher = true,
	           Icon="@drawable/Icon",
	           ScreenOrientation=ScreenOrientation.Portrait)]
	public class Activity1 : Activity
	{
		//public static string TAG = "inAppBillingDemo";
		public static int points;
		public static int level_index;
		public static Activity1 MainlayoutActivity;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);
			
			MainlayoutActivity = this;

		//	LoadTapForTap(this,"cd0c81777ef5a0ec19a7760709b2e327");
			LoadAppay (this, "PMfcxnUdXAKwazCqALsW");

			GameManager.InitiateGame ();
		
			AppRater.app_launched(this);
	//		HeyZap.LoadHeyZap(this,true);
		}

		private static void LoadAppay(Context con, string val)
		{
			try
			{
				IntPtr DeviceIdentifier = JNIEnv.FindClass("com/edealya/lib/DeviceIdentifier");
				IntPtr initialize  = JNIEnv.GetMethodID(DeviceIdentifier,"DeviceIdentifier","(Landroid/content/Context;Ljava/lang/String;)V");
				Java.Lang.String str = new Java.Lang.String(val);
				JNIEnv.CallVoidMethod(DeviceIdentifier,initialize,new JValue(con),new JValue(str));
				IntPtr update = JNIEnv.GetMethodID(DeviceIdentifier, "update","()V");
				JNIEnv.CallVoidMethod(DeviceIdentifier, update);
			}catch(System.Exception e){
				Console.WriteLine(e.ToString());
			}
		}

		private static void LoadTapForTap (Context con,string val)
		{
			try
			{
			IntPtr TapForTap = JNIEnv.FindClass("com/tapfortap/TapForTap");
			//Console.WriteLine(TapForTap.ToString());
			IntPtr initialize  = JNIEnv.GetStaticMethodID(TapForTap,"initialize","(Landroid/content/Context;Ljava/lang/String;)V");
			//Console.WriteLine(initialize.ToString());
			Java.Lang.String str = new Java.Lang.String(val);
			 JNIEnv.CallStaticVoidMethod(TapForTap,initialize,new JValue(con),new JValue(str));
		
			/*IntPtr Interstitial = JNIEnv.FindClass("com/tapfortap/Interstitial");
			IntPtr Interstitial_prepare = JNIEnv.GetStaticMethodID(Interstitial,"prepare","(Landroid/content/Context;)V");
			IntPtr Interstitial_show = JNIEnv.GetStaticMethodID(Interstitial,"show","(Landroid/content/Context;)V");
			JNIEnv.CallStaticVoidMethod(Interstitial,Interstitial_prepare,new JValue(con));
			JNIEnv.CallStaticVoidMethod(Interstitial,Interstitial_show,new JValue(con));
		
			IntPtr AppWall = JNIEnv.FindClass("com/tapfortap/AppWall");
			IntPtr AppWall_prepare = JNIEnv.GetStaticMethodID(AppWall,"prepare","(Landroid/content/Context;)V");
			IntPtr AppWall_show = JNIEnv.GetStaticMethodID(AppWall,"show","(Landroid/content/Context;)V");
			JNIEnv.CallStaticVoidMethod(AppWall,AppWall_prepare,new JValue(con));
			JNIEnv.CallStaticVoidMethod(AppWall,AppWall_show,new JValue(con));*/
			}catch(System.Exception e){
				Console.WriteLine(e.ToString());
			}
		}
        


		protected override void OnStart()
		{
			base.OnStart();
			ResponseHandler.Register(GameManager.purchaser._inAppBillingDemoObserver);
		
		}
		
		protected override void OnStop()
		{
			base.OnStop();

		
		}

		public override void OnBackPressed ()
		{
			if (GameManager.currentGameState == GameState.Level) {
				LayoutManager.ClearLevelLayout();
				GameManager.GetAllLevel(GameManager.currentLevelType,LayoutManager.packageIndex);
				LayoutManager.InitiateLevelSelectionLayout(GameManager.levelList,GameManager.currentLevelType);
				//GameManager.currentGameState=GameState.Selection;
			} else if (GameManager.currentGameState == GameState.Selection) {
				LayoutManager.ClearSelectionLayout();
				LayoutManager.InitiateMenuLayout();
			} else {
				base.OnBackPressed();
			}

		}
		
		protected override void OnDestroy()
		{
			base.OnDestroy();
			ResponseHandler.Unregister(GameManager.purchaser._inAppBillingDemoObserver);
			GameManager.purchaser._purchaseDatabase.Close();
			GameManager.purchaser._billingService.Unbind();
			Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
		}
		
		/**
         * Save the context of the log so simple things like rotation will not
         * result in the log being cleared.
         */
		protected override void OnSaveInstanceState(Bundle outState)
		{
			base.OnSaveInstanceState(outState);
			//outState.putString(LOG_TEXT_KEY, Html.toHtml((Spanned)mLogTextView.getText()));
		}
		
		/**
         * Restore the contents of the log if it has previously been saved.
         */
		protected override void OnRestoreInstanceState(Bundle savedInstanceState)
		{
			base.OnRestoreInstanceState(savedInstanceState);
			
			//if (savedInstanceState != null)
			//mLogTextView.setText(Html.fromHtml(savedInstanceState.getString(LOG_TEXT_KEY)));
		}
	
	
	}

}


