
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;

namespace GuessTheWord
{
	class ProductListAdapter:BaseAdapter<Levels>
	{
		Activity context;
		public List<Levels> listProducts;
		public LevelType levelType;

		public ProductListAdapter (Activity context, List<Levels> lProducts,LevelType lType): base()
		{
			this.context = context;
			this.listProducts = lProducts;
			this.levelType = lType;
		}

		public override int Count {
			get { return this.listProducts.Count; }
		}

		public override Levels this [int position] {
			get { return this.listProducts [position]; }
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{

			var item = this.listProducts [position];
			var view = convertView;
			view = context.LayoutInflater.Inflate (Resource.Layout.LevelTemplate, parent, false);
			if (item.isCompleted) {
				view.SetBackgroundResource (Resource.Drawable.button1_white);
			} else {
				view.SetBackgroundResource (Resource.Drawable.button1_gallery);
			}
			//view.SetBackgroundDrawable(GameManager.GetThumbnailFromAssets(item.word,levelType));
			var imageItem = view.FindViewById (Resource.Id.itemImage) as ImageView;
			var textTop = view.FindViewById (Resource.Id.itemText) as TextView;
			//	var textBottom = view.FindViewById (Resource.Id.textBottom) as TextView;
			imageItem.SetImageBitmap (GameManager.GetThumbnailFromAssets (item.word, GameManager.currentLevelType)[0]);// .SetImageResource (item.Image);
			//	if(item.word!=null)
			textTop.SetText ("Completed", TextView.BufferType.Normal);
			textTop.SetTextColor (Color.Green);
			textTop.Typeface = LayoutManager.font;
			textTop.BringToFront ();

			if (item.isCompleted) {
				textTop.Visibility = ViewStates.Visible;
				imageItem.Visibility = ViewStates.Gone;
			} else {
				textTop.Visibility = ViewStates.Gone;
				imageItem.Visibility = ViewStates.Visible;
			}

		//	textBottom.SetText (item.Description, TextView.BufferType.Normal);
			return view;
		}

		public override long GetItemId (int position)
		{
			return position;
		}
	}

	class Buttons
	{
		public string buttonName{
			get;set;
		}
		public int buttonId{
			get;set;
		}
	}

	class PackageList:BaseAdapter<string>
	{
		Activity context;
		public List<string> buttons;
		
		public PackageList (Activity context, List<string> lProducts): base()
		{
			this.context = context;
			this.buttons = lProducts;
			//this.levelType = lType;
		}
		
		public override int Count {
			get { return this.buttons.Count; }
		}
		
		public override string this [int position] {
			get { return this.buttons [position]; }
		}
		
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			
			var item = this.buttons [position];
			var view = convertView;
			view = context.LayoutInflater.Inflate (Resource.Layout.EmptyButton, parent, false);
			var imageItem = view.FindViewById (Resource.Id.levelPackage) as Button;
			imageItem.SetText(item,TextView.BufferType.Normal);
			imageItem.Typeface = LayoutManager.font;
			imageItem.SetTextColor(Color.White);
			imageItem.Id=800+position;
			LayoutManager.Selector(imageItem,Resource.Drawable.button1,
			                       Resource.Drawable.button1_pressed,
			                       Resource.Drawable.button1_disabled);

			imageItem.Click+=(sender, e) => {
				try
				{
				LayoutManager.packageIndex=position+1;
				Console.WriteLine(position);
				LayoutManager.layout.RemoveView(LayoutManager.menu);
				LayoutManager.menu=null;
				GameManager.GetAllLevel(GameManager.currentLevelType,LayoutManager.packageIndex);
				LayoutManager.InitiateLevelSelectionLayout(GameManager.levelList,GameManager.currentLevelType);
				LayoutManager.dialogX.Dismiss();
				LayoutManager.dialogX.Dispose();
				}
				catch(System.Exception ex){
					Console.WriteLine(ex.ToString());
				}
			};
			//	textBottom.SetText (item.Description, TextView.BufferType.Normal);
			return view;
		}
		
		public override long GetItemId (int position)
		{
			return position;
		}
	}
}

