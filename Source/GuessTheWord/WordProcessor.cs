
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GuessTheWord
{
	class WordProcessor
	{
		public static bool IsEqual(string usertyped,string word)
		{

			return usertyped.Equals(word);
		}

		private static string RandomString(int size, bool lowerCase)
		{
			StringBuilder builder = new StringBuilder();
			Random random = new Random();
			char ch;
			for (int i = 1; i < size+1; i++)
			{
				ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
				builder.Append(ch);
			}
			if (lowerCase)
				return builder.ToString().ToLower();
			else
				return builder.ToString().ToUpper();
		}

		public static char[] GenerateRandomCharArrayForButtons (string word,int numBtn)
		{
			char[] val = new char[numBtn];
			char[] temp = word.ToCharArray ();

			for(int x =0;x<temp.Length;x++) {
				val[x]  = temp[x];
			}

			int numberOfChartoFill = numBtn - temp.Length;
			string rndString = RandomString(numberOfChartoFill,false);
			temp = rndString.ToCharArray();

			int counter=0;
			for(int x =word.Length;x<numBtn;x++) {
				val[x]  = temp[counter++];
			}
			return val ;
		}

		public static char[] FilterMainString (string array, string word)
		{
			char[] filteredArray = array.ToCharArray();

			for(int x=0;x<array.Length;x++)
				for(int y =0;y<word.Length;y++)
					if(array[x]==word[y])
						filteredArray[x]='@';


			return filteredArray;

		}
	}
}

