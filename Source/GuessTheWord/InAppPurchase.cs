using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Billing;
using Android.Util;
using Android.Database;
using Java.Lang;
using System.Collections.Generic;

namespace GuessTheWord
{
	class InAppPurchase : Activity1
	{
		//public TextView ItemName;
		public InAppPurchase()
		{
				StartBillingStuff();
				Android.Runtime.AndroidEnvironment.UnhandledExceptionRaiser += new EventHandler<RaiseThrowableEventArgs>(AndroidEnvironment_UnhandledExceptionRaiser);

		}
		//public static string TAG = "inAppBillingDemo";
		public BillingService _billingService;
		public PurchaseDatabase _purchaseDatabase;
		public InAppBillingDemoPurchaseObserver _inAppBillingDemoObserver;
		public Handler _handler;
	//	public ICursor OwnedItemsCursor;
	//	private SimpleCursorAdapter _ownedItemsAdapter;
		public static string DB_INITIALIZED = "db_initialized";
		public HashSet<string> OwnedItems = new HashSet<string>();

		private void StartBillingStuff()
		{
			_handler = new Handler();
			_inAppBillingDemoObserver = new InAppBillingDemoPurchaseObserver(LayoutManager.activityLayout, _handler);
			_billingService = new BillingService();
			_billingService.Context = LayoutManager.activityLayout;
			ResponseHandler.Register(_inAppBillingDemoObserver);
			var inAppsSupported = _billingService.CheckBillingSupportedMethod(Billing.Consts.ITEM_TYPE_INAPP);
			var subscriptionsSupported = _billingService.CheckBillingSupportedMethod(Billing.Consts.ITEM_TYPE_SUBSCRIPTION);
			
			_purchaseDatabase = new PurchaseDatabase(LayoutManager.activityLayout);
		//	OwnedItemsCursor = _purchaseDatabase.QueryAllPurchasedItems();
		//	StartManagingCursor(OwnedItemsCursor);
		//	var from = new string[] { PurchaseDatabase.PURCHASED_PRODUCT_ID_COL, PurchaseDatabase.PURCHASED_QUANTITY_COL };
			//var to = new int[] { Resource.Id.itemName };
		//	_ownedItemsAdapter = new SimpleCursorAdapter(LayoutManager.activityLayout, Resource.Layout.Main, OwnedItemsCursor, from, to);
			
			//if (OwnedItems.Count == 0)
			//	ItemName.Text = "No purchases";
		}
		
		/**
         * If the database has not been initialized, we send a
         * RESTORE_TRANSACTIONS request to Android Market to get the list of purchased items
         * for this user. This happens if the application has just been installed
         * or the user wiped data. We do not want to do this on every startup, rather, we want to do
         * only when the database needs to be initialized.
         */
//		public void RestoreDatabase()
//		{
		//	var prefs = GetPreferences(FileCreationMode.Private);
		//	var initialized = prefs.GetBoolean(DB_INITIALIZED, false);
			
		//	if (!initialized)
		//	{
		//		_billingService.RestoreTransactionsMethod();
		//		Toast.MakeText(this, "Restoring transactions", ToastLength.Long).Show();
			//}
//		}
		
		/**
 * Creates a background thread that reads the database and initializes the
 * set of owned items.
 */
//		private void InitializeOwnedItems()
//		{
//			new Thread(new Runnable(() =>
//			                        {
//				{
//					DoInitializeOwnedItems();
//				}
//			})).Start();
//		}
		
		/**
 * Reads the set of purchased items from the database in a background thread
 * and then adds those items to the set of owned items in the main UI
 * thread.
 */
/*		private void DoInitializeOwnedItems()
		{
			ICursor cursor = _purchaseDatabase.QueryAllPurchasedItems();
			
			if (cursor == null)
			{
				return;
			}
			
			var ownedItems = new HashSet<string>();
			
			try
			{
				int productIdCol = cursor.GetColumnIndexOrThrow(PurchaseDatabase.PURCHASED_PRODUCT_ID_COL);
				
				while (cursor.MoveToNext())
				{
					var productId = cursor.GetString(productIdCol);
					ownedItems.Add(productId);
				//	ItemName.Text = productId;
				}
			}
			finally
			{
				cursor.Close();
			}
			
			// We will add the set of owned items in a new Runnable that runs on
			// the UI thread so that we don't need to synchronize access to
			// mOwnedItems.
			_handler.Post(new Runnable(() =>
			                           {
				foreach (var item in ownedItems)
				{
				
				}
			}));
		}*/
		
		private void AndroidEnvironment_UnhandledExceptionRaiser(object sender, RaiseThrowableEventArgs e)
		{
			
		}
		
		public void InAppBilling(string itemId)
		{
			var worked = _billingService.RequestPurchaseMethod(itemId,Billing.Consts.ITEM_TYPE_INAPP, null);
		}
		
	
	}
	public class InAppBillingDemoPurchaseObserver : PurchaseObserver
	{
		private Activity1 _activity;
		
		public InAppBillingDemoPurchaseObserver(Activity1 activity, Handler handler)
			: base(activity, handler)
		{
			_activity = activity;
		}
		
		public override void OnBillingSupported(bool supported, string type)
		{
			if (Billing.Consts.DEBUG)
			{
			//	Log.Info(Activity1.TAG, "supported: " + supported);
			}
			if (type == null || type.Equals(Billing.Consts.ITEM_TYPE_INAPP))
			{
				if (supported)
				{
				//	_activity.RestoreDatabase();
				}
				else
				{
					// In app products not supported
				}
			}
			else if (type.Equals(Billing.Consts.ITEM_TYPE_SUBSCRIPTION))
			{
			}
			else
			{
				// Subscriptions not supported
			}
		}
		
		public override void OnPurchaseStateChange(Billing.Consts.PurchaseState purchaseState, string itemId,
		                                           int quantity, long purchaseTime, string developerPayload)
		{

			
			if (purchaseState == Billing.Consts.PurchaseState.PURCHASED)
			{
			//	_activity.OwnedItems.Add(itemId);
			//	_activity.ItemName.Text = itemId;
				switch(GameManager.productID)
				{
				case(1):GameManager.points+=1500;
					LayoutManager.MessageBox("Item Purchased");
					GameManager.UpdateStatus();
					break;
				case(2):GameManager.points+=4000;
					LayoutManager.MessageBox("Item Purchased");
					GameManager.UpdateStatus();
					break;
				case(3):GameManager.points+=8000;
					LayoutManager.MessageBox("Item Purchased");
					GameManager.UpdateStatus();
					break;
				case(4):GameManager.points+=10000;
					LayoutManager.MessageBox("Item Purchased");
					GameManager.UpdateStatus();
					break;
				default:
					LayoutManager.MessageBox("Invalid Item");
					break;
				}
				GameManager.UpdateStatus();
			//	GameManager.productID=;

				GameManager.productID=0;
			}
			
			if (purchaseState == Billing.Consts.PurchaseState.REFUNDED)
			{
			}
			
			if (purchaseState == Billing.Consts.PurchaseState.CANCELED)
			{
			}
			
		//	_activity.OwnedItemsCursor.Requery();
		}
		
		public override void OnRequestPurchaseResponse (BillingService.RequestPurchase request, Billing.Consts.ResponseCode responseCode)
		{
			//if (Billing.Consts.DEBUG) {
			//}//	Log.Debug(Activity1.TAG, request.mProductId + ": " + responseCode);
			
			if (responseCode == Billing.Consts.ResponseCode.RESULT_OK)
			{
				//if (Billing.Consts.DEBUG)
				//			Log.Info(Activity1.TAG, "purchase was successfully sent to server");
			}
			else if (responseCode == Billing.Consts.ResponseCode.RESULT_USER_CANCELED)
			{
				//if (Billing.Consts.DEBUG)
				//			Log.Info(Activity1.TAG, "user canceled purchase");
			}
			else
			{
				//if (Billing.Consts.DEBUG)
				//	Log.Info(Activity1.TAG, "purchase failed");
			}
		}
		
		public override void OnRestoreTransactionsResponse(BillingService.RestoreTransactions request, Billing.Consts.ResponseCode responseCode)
		{
			if (responseCode == Billing.Consts.ResponseCode.RESULT_OK)
			{
				//if (Billing.Consts.DEBUG)
				//	Log.Debug(Activity1.TAG, "completed RestoreTransactions request");
				
				// Update the shared preferences so that we don't perform
				// a RestoreTransactions again.
				//ISharedPreferences prefs = _activity.GetPreferences(FileCreationMode.Private);
			//	ISharedPreferencesEditor edit = prefs.Edit();
			//	edit.PutBoolean(Activity1.DB_INITIALIZED, true);
			//	edit.Commit();
			}
			else
			{
				//if (Billing.Consts.DEBUG)
				//	Log.Debug(Activity1.TAG, "RestoreTransactions error: " + responseCode);
			}
		}
	}
}

